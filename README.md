# Setup

1. Ensure Docker is installed
2. Clone this repo
3. Build the Docker image using `docker build -t comp_couchdb couchdb`
4. Run the container using `docker run -d -p 5984:5984 comp_couchdb`

# Usage
Your application should be able to access the server at `localhost:5984`

The username will be `admin`, and the password will be `Vai9vah6`

In addition, to access the graphical web interface for the database, browse to <http://localhost:5984/_utils>
